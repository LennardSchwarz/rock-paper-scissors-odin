const selection = ['rock', 'paper', 'scissors'];
const YOUWIN = 'You win! ';
const YOULOSE = 'You lose! ';
const TIE = 'Tie';

const computerSelection = () => {
    return selection[Math.floor(Math.random() * selection.length)]
};

const playRound = (playerSelection) => {
    const computerPlay = computerSelection();

    if (computerPlay === playerSelection) {
        let output = TIE;
        return {
            result: output,
            player: playerSelection,
            computer: computerPlay
        };
    } else if (computerPlay === 'rock') {
        if (playerSelection === 'paper') {
            return {
                result: YOUWIN,
                player: playerSelection,
                computer: computerPlay
            };
        }
        else if (playerSelection === 'scissors'){
            return {
                result: YOULOSE,
                player: playerSelection,
                computer: computerPlay
            };
        }
    } else if (computerPlay === 'paper') {
        if (playerSelection === 'rock') {
            return {
                result: YOULOSE,
                player: playerSelection,
                computer: computerPlay
            };
        }
        else if (playerSelection === 'scissors') {
            return {
                result: YOUWIN,
                player: playerSelection,
                computer: computerPlay
            };
        }
    } else if (computerPlay === 'scissors') {
        if (playerSelection === 'rock') {
            return {
                result: YOUWIN,
                player: playerSelection,
                computer: computerPlay
            };
        }
        else if (playerSelection === 'paper') {
            return {
                result: YOULOSE,
                player: playerSelection,
                computer: computerPlay
            };
        }
    }
};

const handleClick = (e) => {
    if (e.target.matches('.playerInput')) {
        let playerSelection = e.target.id;
        gameLogic(playerSelection);
    }
};

const score = {wins: 0, losses: 0, ties: 0};

const animateTiles =(oneRoundResult) => {
    //reset tile animation
    const animations = ['playerTileAnimation', 'computerTileAnimation', 'TieTileAnimation'];
    const tiles = document.querySelectorAll('.playerInput');
    let removeAnimation= () => tiles.forEach(tile => tile.classList.remove(...animations));
    removeAnimation();

    let computer =  document.querySelector(`#${oneRoundResult.computer}`);
    let player = document.querySelector(`#${oneRoundResult.player}`);

    if (computer === player) {
        computer.classList.add('TieTileAnimation');
    } else {
        computer.classList.add('computerTileAnimation');
        player.classList.add('playerTileAnimation');
    }

    setTimeout(removeAnimation, 2000);



};

const gameLogic = (playerSelection) => {
  let oneRoundResult = playRound(playerSelection);
  animateTiles(oneRoundResult); //trigger animation
  let result = oneRoundResult.result;
  switch (result) {
    case YOUWIN:
      score.wins++;
      updateScore(oneRoundResult);
      break;
    case YOULOSE:
      score.losses++;
      updateScore(oneRoundResult);
      break;
    case "Tie!":
      score.ties++;
      updateScore(oneRoundResult);
      break;
  }
};

const updateScore = (oneRoundResult) => {
    const win = document.querySelector('#wct');
    const lose = document.querySelector('#lct');
    const tie = document.querySelector('#tct');

    //set new score
    win.textContent = score.wins;
    lose.textContent = score.losses;
    tie.textContent = score.ties;

    //change #gametext
    const gametxtPara = document.querySelector('#gametxtPara');
    const result = oneRoundResult.result;
    const player = oneRoundResult.player;
    const computer = oneRoundResult.computer;
    let gametxt = `${result}
    
    You chose: ${player}
    
    Computer chose: ${computer}`; //TODO:
    gametxtPara.textContent = gametxt;


};

const reset = () => {
    window.location.reload();
};

window.addEventListener('click', handleClick);
