Little Rock, Paper, Scissors game.

You can play it [here](https://lennardschwarz.gitlab.io/rock-paper-scissors-odin)!

Or put the following link in the browser bar:
https://lennardschwarz.gitlab.io/rock-paper-scissors-odin